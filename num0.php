<?php
//SCRIPT DE CONTADOR EN CEROS CON JSON

//Se llama al archivo JSON a.json
$data = file_get_contents('a.json'); 

//Guarda el contenido del archivo en un arreglo de PHP
$json_arr = json_decode($data, true);

//Se extrae el número del archivo
$no=$json_arr[0]['Num'];

//print_r($json_arr);

//Se suma al número anterior
$no = 0;

//El número se guarda en un arreglo para JSON
$arr=array(array('Num'=>$no));

//print_r($arr);

//El nuevo arreglo se guarda en el archivo JSON
file_put_contents('a.json', json_encode($arr,JSON_PRETTY_PRINT));


?>