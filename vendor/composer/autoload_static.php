<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit19517a8d32bc42f53607acd40ca6635d
{
    public static $prefixLengthsPsr4 = array (
        'P' => 
        array (
            'PHPMailer\\PHPMailer\\' => 20,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'PHPMailer\\PHPMailer\\' => 
        array (
            0 => __DIR__ . '/..' . '/phpmailer/phpmailer/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit19517a8d32bc42f53607acd40ca6635d::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit19517a8d32bc42f53607acd40ca6635d::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
