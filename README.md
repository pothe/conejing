# Plugin y Pings

**Este repo es de catar cosas de PHP**

## Repo con archivos script desencarpetados temporalmente porque es para implementarlos via SSH en nuve.ml

### El plugin de WP se encuentra en la carpeta "Plugin"

## En esta carpeta principal se encuentran los archivos para catar los ping local y remoto:
Archivo | Descripci�n del archivo
---- | -----
index.php | PHP principal que llama a los ping
ping.php  | PHP de ping local
pong.php  | PHP para hacer un ping a una p�gina o servidor externo
ping.php  | PHP de ping local
num.php   | PHP de contador al realizar un ping
num0.php  | PHP que pone el cero el contador de num.php
correo.php| PHP que envia un correo al llegar a 5 el contador
a.json    | JSON que guarda el contador
vendor    | Paquete que envia el email

## Falta hacer:
- [ ] Encarpetar el script 
- [ ] Documentar el script
- [ ] Enviar script a Repo de ScriPHP
- [ ] Ver que hacer con este repo

Script PHP desarrollado por **Marco Antonio Castillo Reyes**